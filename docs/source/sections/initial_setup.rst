.. _installation:

Installation
============

You can install using pip

.. code-block:: console
 
	pip install pyshell

You can also install using the Volian repo

.. code-block:: console
 
	echo "deb http://deb.volian.org/volian/ scar main" | sudo tee /etc/apt/sources.list.d/volian-archive-scar-unstable.list
	wget -qO - https://deb.volian.org/volian/scar.key | sudo tee /etc/apt/trusted.gpg.d/volian-archive-scar-unstable.gpg > /dev/null
	sudo apt update && sudo apt install python3-pyshell

.. _initialization:

Initialization
--------------

Now that it's installed you can import and initialize:

.. code-block:: python

	from pyshell import pyshell

	shell = pyshell(logfile='/tmp/shell.log', shell='/bin/bash')
	shell.echo('Hello', 'World')
	
.. seealso:: :ref:`pyshell_arguments`